# MALIMO (MAc LIfe MOnitor)

## Overview

Macでの自分の行動を記録するツールです．ウィンドウ切替時に， 切り替えら
れたアプリケーション名とウィンドウタイトル，その時間を記録します． あと
から，自分の行動を振り返れるようになります． Windows向けの
[Taskpit](http://taskpit.jpn.org)をMac用に移植したものです．

This application stores activity logs on your Mac. An activity log is
logged at switching window, is application name, window title, and its
time. The activity logs is for reviewing myself. The concept of this
application is ported from [Taskpit](http://taskpit.jpn.org) for Windows.

## Requirements

* 動作環境に Mac OX Mountain Lion 以上が必要です．
* Mac OS X Mountain Lion or Later for Running.

## Download

* [MALIMO 1.00](https://bitbucket.org/sfujiwara/malimo/downloads/MALIMO_1.00.dmg) for Mountain Lion or Later.

[Go to download page](http://bitbucket.org/sfujiwara/malimo/downloads)

# Authors of MALIMO project

* Developers
    * Shin Fujiwara (B4, Kyoto Sangyo University)
    * Tetsuya Ohdo (M2, Graduate School of Kyoto Sangyo University)
    * Atsushi Itsuda (M1, Graduate School of Kyoto Sangyo University)
* Homepage Design
    * Ryuki Otaki (B4, Kyoto Sangyo University)
* Supervisor
    * Haruaki Tamada (Associate Professor, Kyoto Sangyo University)

MALIMOの初期バージョンは京都産業大学 コンピュータ理工学部 玉田研で開発
されました．

Original developers of MALIMO belong Tamada Lab., Faculty of Computer
Science and Engineering, Kyoto Sangyo University.

# License

MALIMOはオープンソース・ソフトウェアです．使用に制限はありません． MALIMOのライセンスは Apache License 2.0 です．．

MALIMO is OSS, free for use (no charge). License of MALIMO is Apache License 2.0.

    Copyright 2014- MALIMO project

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
