//
//  Setting.m
//  MALIMO
//
//  Created by fujiwara shin on 2013/04/09.
//  Copyright (c) 2013年 fujiwara shin. All rights reserved.
//

#import "Setting.h"

@implementation Setting

-(id)init
{
    self = [super init];
    if(self){
        defaults = [NSUserDefaults standardUserDefaults];
    }
    return self;
}

-(void)setFilePath:(NSString*)saveFilePath{
    [defaults setObject:saveFilePath forKey:@"saveFilePath"];
    [defaults synchronize];
    NSLog(@"書き込みました%@書き込み",[defaults stringForKey:@"saveFilePath"]);
}

-(void)setLoginStart:(BOOL)isLoginStart{
    [defaults setBool:isLoginStart forKey:@"isValid"];
    [defaults synchronize];
}

-(NSString*)getFilePath{
    NSString* pass = [defaults stringForKey:@"saveFilePath"];
    [pass stringByReplacingOccurrencesOfString: @"%20" withString: @" "];
    [pass stringByReplacingOccurrencesOfString: @"%E3%80%80" withString: @"　"];
    return pass;
}

-(BOOL)getIsLoginStart{
    BOOL isLoginStart = [defaults boolForKey:@"isLoginStart"];
    return isLoginStart;
}

@end
