//
//  Recorder.h
//  MALIMO
//
//  Created by fujiwara shin on 2013/04/08.
//  Copyright (c) 2013年 fujiwara shin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Setting.h"

@interface Recorder : NSObject
{
@private
    NSWorkspace *workSpace;

    NSFileManager *fileManager;
    NSFileHandle *fileHandle;
    NSString *deactiveJapanAppName;
    NSString *saveFileName;
    NSDate *startDate;
    Setting *setting;
    id rightClickEvent;
    id leftClickEvent;
    id keyDownEvent;
    
@public
    NSMutableString *filePath;
    int clickCount;
    int keyDownCount;
    BOOL isRun;
    
}
-(void)run;
-(void)stop;
-(void)changeSaveAs;
-(void)enableLoginItem;
-(BOOL)isEnableLoginItem;

@end

