//
//  Logs.h
//  MALIMO
//
//  Created by fujiwara shin on 2013/05/02.
//  Copyright (c) 2013年 fujiwara shin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Setting.h"
#import "Recorder.h"

@interface Log : NSObject{
    NSMutableArray *log;
}
-(void)scanLog;
-(NSArray*)getRankLog;
@end
