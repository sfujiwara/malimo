//
//  Setting.h
//  MALIMO
//
//  Created by fujiwara shin on 2013/04/09.
//  Copyright (c) 2013年 fujiwara shin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Setting : NSObject
{
    @private
    NSUserDefaults* defaults;
    NSMutableString *filePath;

}
-(void)setFilePath:(NSString*)saveFilePath;
-(void)setLoginStart:(BOOL)isLoginStart;
-(NSString*)getFilePath;
-(BOOL)getIsLoginStart;
@end
