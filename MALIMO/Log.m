//
//  Logs.m
//  MALIMO
//
//  Created by fujiwara shin on 2013/05/02.
//  Copyright (c) 2013年 fujiwara shin. All rights reserved.
//

#import "Log.h"

@implementation Log
//現在のファイルパスからlog情報を取得する
-(void)scanLog{
    // UTF8 エンコードされた CSV ファイル

    NSMutableString *filePath = [[NSMutableString alloc]init];
    Setting* setting= [[Setting alloc]init];
    [filePath setString:@"/"];
    NSArray *names = [[setting getFilePath] componentsSeparatedByString:@"/"];
    for(int i = 3;i < [names count];i++){
        [filePath appendString:[names objectAtIndex: i]];
        if(i == [names count]-1)break;
        [filePath appendString:@"/"];
    }
    NSString *text = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    
    log = [[NSMutableArray alloc] init];
    
    // 改行文字で区切って配列に格納する
    NSArray *lines = [text componentsSeparatedByString:@"\n"];

    for (NSString *row in lines) {
        // コンマで区切って配列に格納する
        NSArray *logRow = [row componentsSeparatedByString:@","];
       // NSLog(@"%@",log);
        [log addObject:logRow];

    }

}
-(NSArray*)getRankLog{
    NSMutableArray *rankName = [[NSMutableArray alloc]init];
    NSMutableArray *rankTime = [[NSMutableArray alloc]init];
    @try {
        // 通常の処理
        for(NSArray *row in log){
            NSString *appName = [row objectAtIndex:1];
            NSString *interval = [row objectAtIndex:5];
            if([rankName containsObject:appName]){
                NSInteger indexNumber = [rankName indexOfObject:appName];
                NSNumber *sumTime = [NSNumber numberWithDouble:[interval doubleValue] + [[rankTime objectAtIndex:indexNumber] doubleValue]];
                [rankTime replaceObjectAtIndex:indexNumber withObject:sumTime];
            }else{
                [rankName addObject:appName];
                [rankTime addObject:[NSNumber numberWithDouble:[interval doubleValue]]];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }

    //バブルソート
    /*2つのNSArrayを同時にソートする方法が他にわからなかった*/
    for(NSUInteger i = 0;i < [rankTime count];i++){
        for(NSUInteger j = [rankTime count]-1;j > i;j--){
            if([[rankTime objectAtIndex:j]doubleValue] > [[rankTime objectAtIndex:j-1]doubleValue]){
                NSNumber* swapTime = [[rankTime objectAtIndex:j] copy];
                [rankTime replaceObjectAtIndex:j withObject:[[rankTime objectAtIndex:j-1] copy]];
                [rankTime replaceObjectAtIndex:j-1 withObject:swapTime];
                NSString* swapAppName = [[rankName objectAtIndex:j] copy];
                [rankName replaceObjectAtIndex:j withObject:[[rankName objectAtIndex:j-1] copy]];
                [rankName replaceObjectAtIndex:j-1 withObject:swapAppName];
            }
        }
    }

    NSArray *result = [rankName arrayByAddingObjectsFromArray:rankTime];
    return result;
}

@end
