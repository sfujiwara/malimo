//
//  main.m
//  MALIMO
//
//  Created by fujiwara shin on 2013/04/08.
//  Copyright (c) 2013年 fujiwara shin. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **)argv);
}
