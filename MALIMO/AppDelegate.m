//
//  AppDelegate.m
//  MALIMO
//
//  Created by fujiwara shin on 2013/04/08.
//  Copyright (c) 2013年 fujiwara shin. All rights reserved.
//

#import "AppDelegate.h"
#define SHOW_LOG_ITEM_NUM 5

@implementation AppDelegate


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSStatusBar *statusBar = [NSStatusBar systemStatusBar];
    
    statusItem = [statusBar statusItemWithLength:NSVariableStatusItemLength];
    
    //[status_item setTitle:@"MALIMO"];
    [statusItem setHighlightMode:YES];
    [statusItem setImage:[NSImage imageNamed:@"marimo.png"]];
    //[status_item setAlternateImage:[NSImage imageNamed:@"yorumarimo.png"]];
    [statusItem setMenu:_mainMenu];
    
    recorder = [[Recorder alloc] init];
    log = [[Log alloc] init];
    
    startRecordSelecter = [_menuItemStartRecord action];
    stopRecordSelecter = [_menuItemStopRecord action];
    saveAsSelecter = [_menuItemSaveAs action];
    
    if([recorder isEnableLoginItem]){
        [_menuItemOpenAtLogin setAction:NULL];
        recorder = [recorder init];
        [recorder run];
        [_menuItemSaveAs setAction:NULL];
        [_menuItemStartRecord setAction:NULL];
        [_menuItemStopRecord setAction:stopRecordSelecter];
        [_menuItemStartRecord setState:YES];
        [_menuItemOpenAtLogin setState:YES];
    }else{
        [_menuItemStopRecord setAction:NULL];
    }
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(showStatus:)
     name:NSMenuDidBeginTrackingNotification
     object:nil];

}


- (IBAction)StartRecord:(id)sender {
    
    recorder = [recorder init];
    [recorder run];
    [_menuItemSaveAs setAction:NULL];
    [_menuItemStartRecord setAction:NULL];
    [_menuItemStopRecord setAction:stopRecordSelecter];
    [_menuItemStartRecord setState:YES];
    
}
- (IBAction)StopRecord:(id)sender {
    [recorder stop];
    [_menuItemSaveAs setAction:saveAsSelecter];
    [_menuItemStartRecord setAction:startRecordSelecter];
    [_menuItemStopRecord setAction:NULL];
    [_menuItemStartRecord setState:NO];
}
- (IBAction)SaveAs:(id)sender {
    [recorder changeSaveAs];
}




- (IBAction)OpenAtLogin:(id)sender {
    [recorder enableLoginItem];
    [_menuItemOpenAtLogin setState:YES];
    
    [_menuItemOpenAtLogin setAction:NULL];
}


- (void)showStatus:(NSNotification *)aNotification
{
    [log scanLog];
    [self updateShowLog];
}
-(void)updateShowLog{
    @try {
        NSArray *logArray = [log getRankLog];
        NSArray *showLogItemArray = [NSArray arrayWithObjects:_showLogItem1,_showLogItem2,_showLogItem3,_showLogItem4,_showLogItem5, nil];
        NSNumberFormatter* numberFormatter = [[NSNumberFormatter alloc] init];
        numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        numberFormatter.minimumFractionDigits = 0;
        numberFormatter.maximumFractionDigits = 0;
        for(NSInteger i = 0;i < SHOW_LOG_ITEM_NUM;i++){
            if([logArray count] >= i*2){
                NSString *logName = [logArray objectAtIndex:i];
                NSString *logTime = [numberFormatter stringFromNumber:[logArray objectAtIndex:[logArray count]/2+i]];
                NSString *space = @"";
                for(NSInteger j = 0;j < 40-[logName lengthOfBytesUsingEncoding:NSShiftJISStringEncoding];j++){space = [space stringByAppendingString:@" "];}
                NSString *s = [NSString stringWithFormat:@"%ld : %@    %@%@",i+1,logName,space,logTime];
                [[showLogItemArray objectAtIndex:i] setTitle:s];
            }
        }
    }@catch (NSException *exception) {
    }
}
@end

