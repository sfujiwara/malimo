//
//  Recorder.m
//  MALIMO
//
//  Created by fujiwara shin on 2013/04/08.
//  Copyright (c) 2013年 fujiwara shin. All rights reserved.
//

#import "Recorder.h"

@implementation Recorder

-(id)init
{
    self = [super init];
    if(self){
        setting = [[Setting alloc] init];
        isRun = NO;
        fileManager = [NSFileManager defaultManager];
        if(!filePath){
            filePath = [[NSMutableString alloc] init];
            if([setting getFilePath] == nil){
                [filePath setString:NSHomeDirectory()];
                [filePath appendString:@"/Desktop/MALIMO"];
                [fileManager createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:NULL];
                [filePath appendString:@"/log.csv"];
                saveFileName = @"";
            }else{
        
                NSArray *names = [[setting getFilePath] componentsSeparatedByString:@"/"];

                [filePath setString:@"/"];
                for(int i = 3;i < [names count] -1;i++){
                    [filePath appendString:[names objectAtIndex: i]];
                    [filePath appendString:@"/"];
                }
                if(![fileManager fileExistsAtPath:filePath]){
                    [fileManager createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:NULL];
                }
                saveFileName = [names objectAtIndex:[names count] -1];
            }
        }else{
            fileManager = [NSFileManager defaultManager];
            [fileManager createDirectoryAtPath:filePath withIntermediateDirectories:YES attributes:nil error:NULL];
            if ([filePath hasSuffix:@"/"]){
                [filePath appendString:saveFileName];
            }
        }
        if(![fileManager fileExistsAtPath:filePath]){
            [fileManager createFileAtPath:filePath contents:nil attributes:nil];
        }
        
        fileHandle = [NSFileHandle fileHandleForWritingAtPath:filePath];
        [fileHandle seekToEndOfFile];
        deactiveJapanAppName = nil;
        startDate = [NSDate date];
        workSpace = [NSWorkspace sharedWorkspace];
    }
    return self;
}

-(void)run
{
    isRun = YES;
    [[workSpace notificationCenter]
     addObserver:self
     selector:@selector(showActiveApplication:)
     name:@"NSWorkspaceDidDeactivateApplicationNotification"
     //name: nil
     object:nil];
    
    [NSEvent addGlobalMonitorForEventsMatchingMask:NSLeftMouseDownMask
                                           handler:^(NSEvent* event) {
                                               clickCount++;
                                           }];
    [NSEvent addGlobalMonitorForEventsMatchingMask:NSRightMouseDownMask
                                           handler:^(NSEvent* event) {
                                               clickCount++;
                                           }];
    [NSEvent addGlobalMonitorForEventsMatchingMask:NSKeyDownMask
                                           handler:^(NSEvent* event) {
                                               keyDownCount++;
                                           }];
}

-(void)stop
{
    if(leftClickEvent){
        [NSEvent removeMonitor:leftClickEvent];
    }
    if(rightClickEvent){
        [NSEvent removeMonitor:rightClickEvent];
    }
    if(keyDownEvent){
        [NSEvent removeMonitor:keyDownEvent];
    }
    [[workSpace notificationCenter] removeObserver:self];
    isRun = NO;
}

- (void)showActiveApplication:(NSNotification *)aNotification
{
    
    //    NSError* error;
    NSString* deactiveAppName;
    NSString* activeJapanAppName;
    NSDate* nowDate = [NSDate date];
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY年MM月dd日 HH時mm分ss秒"];
    NSString* stringStartDate = [formatter stringFromDate:startDate];
    NSTimeInterval interval = [nowDate timeIntervalSinceDate:startDate];
    
    //  NSLog(@"name: %@", [aNotification name]);
    //  NSLog(@"object: %@", [aNotification object]);
    NSDictionary* info = [aNotification userInfo];
    activeJapanAppName = [[workSpace activeApplication] valueForKey:@"NSApplicationName"];
    if(info != nil){
        NSArray *name = [[info description] componentsSeparatedByString:@" "];
        deactiveAppName = [name objectAtIndex:8];
        name = [deactiveAppName componentsSeparatedByString:@"."];
        NSUInteger count = 0;
        count = [name count];
        deactiveAppName = [name objectAtIndex:(count-1)];
        NSLog(@"DidDeactiveApplicationName: %@",deactiveAppName);
    }
    
    CFArrayRef list =CGWindowListCopyWindowInfo((kCGWindowListOptionAll|kCGWindowListOptionOnScreenOnly|kCGWindowListExcludeDesktopElements), kCGNullWindowID);
    CFDictionaryRef w;
    CFIndex i;
    
    CGWindowID windowID;
    CFStringRef name;
    CGRect rect;
    Boolean flag = true;
    for (i=0; i < CFArrayGetCount(list) && deactiveJapanAppName!=nil; i++) {
        flag = false;
        w = CFArrayGetValueAtIndex(list, i);
        //   if(CFDictionaryContainsKey(w, kCGWindowWorkspace)){
        CFNumberGetValue(CFDictionaryGetValue(w, kCGWindowNumber),kCGWindowIDCFNumberType, &windowID);
        name = CFDictionaryGetValue(w, kCGWindowName);
	    NSMutableString* namensms = (__bridge_transfer NSMutableString*)name;
	    [namensms replaceOccurrencesOfString:@"," withString: @"、" options:0 range:NSMakeRange( 0 ,[namensms length] )];

        CGRectMakeWithDictionaryRepresentation(CFDictionaryGetValue(w, kCGWindowBounds), &rect);
        CFStringRef winName = CFDictionaryGetValue(w, kCGWindowOwnerName);
        if([deactiveJapanAppName isEqualToString:(__bridge NSString*)winName] || [deactiveAppName isEqualToString:(__bridge NSString*)winName]){
            if([(__bridge NSString*)name length] > 1){
                NSLog(@"log:%@",name);
                flag = true;
                //[str writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
                
                [fileHandle writeData:[[NSString stringWithFormat:@"%@,%@,%@,%d,%d,%f\n",stringStartDate,deactiveJapanAppName,namensms,clickCount,keyDownCount,interval] dataUsingEncoding:NSUTF8StringEncoding]];
                
                //システムログ
                /*
                 openlog("MALIMO_Rec", LOG_CONS | LOG_PID, LOG_USER);
                 syslog(LOG_NOTICE,"%s,%s,%s,%d,%d,%f\n",[stringStartDate UTF8String],[deactiveJapanAppName UTF8String],[(__bridge NSString*)name UTF8String],clickCount,KeyDownCount,interval);
                 closelog();
                 */
                break;
            }
        }
        // }
    }
    if(flag == false){
        NSLog(@"%@",deactiveJapanAppName);
        //[str writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
        
        //loginwindowのときはログを取らない仕様にしてみた by大堂
        if(![deactiveJapanAppName isEqualToString:@"loginwindow"]){
            [fileHandle writeData:[[NSString stringWithFormat:@"%@,%@,%@,%d,%d,%f\n",stringStartDate,deactiveJapanAppName,deactiveJapanAppName,clickCount,keyDownCount,interval] dataUsingEncoding:NSUTF8StringEncoding]];
        }
         
         
    }
    // [fileHandle closeFile];
    
    //    [_deactiveJapanAppName setStringValue:activeJapanAppName];
    keyDownCount=0;
    clickCount=0;
    
    startDate = nowDate;
    deactiveJapanAppName = activeJapanAppName;
}

-(void)changeSaveAs
{
    NSSavePanel *savePanel	= [NSSavePanel savePanel];
    
    NSArray *allowedFileTypes = [NSArray arrayWithObjects:@"csv",@"'CSV'",nil];
    
    [savePanel setAllowedFileTypes:allowedFileTypes];
    
    // ダイアログを表示し、押下されたボタンを取得
    NSInteger pressedButton = [savePanel runModal];
    
    if( pressedButton == NSOKButton )
    {
        //        [fileHandle closeFile];
        filePath = [[NSMutableString alloc] init];
        NSString *stringFilePath = [[savePanel URL] absoluteString];
        
        [filePath setString:[[savePanel URL] absoluteString]];
        
        
                
        NSLog(@"file saved '%@'", filePath);
        [setting setFilePath:filePath];
        if(![fileManager fileExistsAtPath:filePath]){
            NSLog(@"createFilePath");
            [fileManager createFileAtPath:filePath contents:nil attributes:nil];
        }  

        NSArray *names = [stringFilePath componentsSeparatedByString:@"/"];
        [filePath setString:@"/"];
        for(int i = 3;i < [names count] -1;i++){
            [filePath appendString:[names objectAtIndex: i]];
            [filePath appendString:@"/"];
        }
        
        [filePath replaceOccurrencesOfString:@"%20" withString: @" " options:0 range:NSMakeRange( 0 , [filePath length] )];
        [filePath replaceOccurrencesOfString:@"%E3%80%80" withString: @"　" options:0 range:NSMakeRange( 0 , [filePath length] )];

        
        saveFileName = [names objectAtIndex:[names count] -1];
        NSLog(@"%@",saveFileName);
        
        NSLog(@"file saved '%@'", filePath);
        
    }
    else if( pressedButton == NSCancelButton )
    {
     	NSLog(@"Cancel button was pressed.");
    }
    else
    {
     	// error
    }
}

- (void)enableLoginItem
{
    // 自分自身のバンドルパスを取得
    CFURLRef appBundleURL = (__bridge CFURLRef)[NSURL fileURLWithPath: [[NSBundle mainBundle] bundlePath]];
    // ログイン項目のリストを取得
    LSSharedFileListRef currentLoginItems = LSSharedFileListCreate(NULL, kLSSharedFileListSessionLoginItems, NULL);
    // ログイン項目をリストに追加
    LSSharedFileListItemRef addedLoginItem = LSSharedFileListInsertItemURL(currentLoginItems, kLSSharedFileListItemLast, NULL, NULL, appBundleURL, NULL, NULL);
    // 追加されたログイン項目を解放
    if (addedLoginItem) {
        CFRelease(addedLoginItem);
    }
    // ログイン項目のリストを解放
    CFRelease(currentLoginItems);
    NSLog(@"malimoをログイン項目に追加したぜ！");
}
- (BOOL)isEnableLoginItem
{
    BOOL enabled = NO;
    UInt32 aSnapshotSeed;
    
    // 自分自身のバンドルパスを取得
    CFURLRef appBundleURL = (__bridge CFURLRef)[NSURL fileURLWithPath: [[NSBundle mainBundle] bundlePath]];
    // ログイン項目のリストを取得
    LSSharedFileListRef currentLoginItems = LSSharedFileListCreate(NULL, kLSSharedFileListSessionLoginItems, NULL);
    // ログイン項目のスナップショットリストを取得
    NSArray *snapshotLoginItems = (__bridge NSArray *)LSSharedFileListCopySnapshot(currentLoginItems, &aSnapshotSeed);
    // ログイン項目のスナップショットリストを走査
    for (id snapshotLoginItem in snapshotLoginItems) {
        // ログイン項目とファイルパスの解決
        if (LSSharedFileListItemResolve((__bridge LSSharedFileListItemRef)snapshotLoginItem, 0, (CFURLRef *)&appBundleURL, NULL) == noErr) {
            // バンドルパスが一致
            if ([[(__bridge NSURL *)appBundleURL path] hasPrefix:[[NSBundle mainBundle] bundlePath]]) {
                // 戻り値設定
                enabled = YES;
                break;
            }
        }
    }
    
    CFRelease(currentLoginItems);
    
    return enabled;
}

@end