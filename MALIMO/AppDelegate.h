//
//  AppDelegate.h
//  MALIMO
//
//  Created by fujiwara shin on 2013/04/08.
//  Copyright (c) 2013年 fujiwara shin. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Recorder.h"
#import "Log.h"

@interface AppDelegate : NSObject <NSApplicationDelegate>{
    NSStatusItem *statusItem;

    
    __weak NSMenu *_mainMenu;
    __weak NSMenuItem *_menuItemStartRecord;
    __weak NSMenuItem *_menuItemStopRecord;
    __weak NSMenuItem *_menuItemSaveAs;
    __weak NSMenuItem *_menuItemOpenAtLogin;
    __weak NSMenuItem *_showLog;
    __weak NSMenuItem *_showLogItem1;
    __weak NSMenuItem *_showLogItem2;
    __weak NSMenuItem *_showLogItem3;
    __weak NSMenuItem *_showLogItem4;
    __weak NSMenuItem *_showLogItem5;
    
    __weak NSMenu *_statusMenu;

    
    SEL startRecordSelecter;
    SEL stopRecordSelecter;
    SEL saveAsSelecter;
    
    Recorder *recorder;
    Log *log;
}

- (IBAction)StartRecord:(id)sender;
- (IBAction)StopRecord:(id)sender;
- (IBAction)SaveAs:(id)sender;
- (IBAction)OpenAtLogin:(id)sender;
- (void)showStatus:(NSNotification *)aNotification;
-(void)updateShowLog;

//@property (weak) IBOutlet NSMenuItem *menuItemStartRecord;
//@property (weak) IBOutlet NSMenuItem *menuItemStopRecord;
//@property (weak) IBOutlet NSMenuItem *menuItemSaveAs;
//@property (weak) IBOutlet NSMenuItem *menuItemShowStatus;

//@property (weak) IBOutlet NSMenu *main_menu;
@property (weak) IBOutlet NSMenuItem *openAtLogin;
@property (weak) IBOutlet NSMenuItem *menuItemOpenAtLogin;
@property (weak) IBOutlet NSMenu *statusMenu;
//@property (weak) IBOutlet NSMenuItem *showLog;

@property (weak) IBOutlet NSMenuItem *showLogItem1;
@property (weak) IBOutlet NSMenuItem *showLogItem2;
@property (weak) IBOutlet NSMenuItem *showLogItem3;
@property (weak) IBOutlet NSMenuItem *showLogItem4;
@property (weak) IBOutlet NSMenuItem *showLogItem5;
@end
